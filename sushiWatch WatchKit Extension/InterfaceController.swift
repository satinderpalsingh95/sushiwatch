//
//  InterfaceController.swift
//  sushiWatch WatchKit Extension
//
//  Created by Satinder pal Singh on 2019-10-30.
//  Copyright © 2019 Satinder pal Singh. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    var countPause = 0;
    @IBOutlet weak var timeLeft: WKInterfaceLabel!
    @IBOutlet weak var powerUpButton: WKInterfaceButton!
    @IBOutlet weak var leftButton: WKInterfaceButton!
    @IBOutlet weak var rightButton: WKInterfaceButton!
    
    @IBOutlet weak var saveButton: WKInterfaceButton!
    @IBOutlet weak var pauseButtonOutlet: WKInterfaceButton!
    @IBAction func pauseButton() {
        if (WCSession.default.isReachable) {
            
            if (countPause % 2 == 0)
            {
                let message = ["side" : "none","powerup": 0] as [String : Any]
                rightButton.setHidden(true)
                leftButton.setHidden(true)
                powerUpButton.setHidden(true)
                timeLeft.setHidden(true)
                WCSession.default.sendMessage(message, replyHandler: nil)
            }
            else{
                
                timeLeft.setHidden(false)
                rightButton.setHidden(false)
                leftButton.setHidden(false)
                let message = ["side" : "none","powerup": 0] as [String : Any]
                WCSession.default.sendMessage(message, replyHandler: nil)
            }
            countPause = countPause + 1
        }
    }
    @IBAction func powerUpButtonPressed() {
        print("inside power up")
        powerUpButton.setHidden(true)
        if (WCSession.default.isReachable) {
            let message = ["side" : "none","powerup": 2] as [String : Any]
            
            WCSession.default.sendMessage(message, replyHandler: nil)
            
        }
        
        
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        saveButton.setHidden(true)
        powerUpButton.setHidden(true)
        if (WCSession.isSupported() == true) {
            print("WC supproted")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("WC not supported")
        }
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        let counter = message["timeRemaining"] as! Int
        if(counter == 15 || counter == 10 || counter == 5 )
        {
            timeLeft.setText("Time Left : \(counter)")
        }
        else if(counter == 0){
            timeLeft.setText("Game Up!!")
            rightButton.setHidden(true)
            leftButton.setHidden(true)
            powerUpButton.setHidden(true)
            saveButton.setHidden(false)
            pauseButtonOutlet.setHidden(true)
            
        }
        else if (counter < 0 ) {
//            timeLeft.setHidden(false)
//            rightButton.setHidden(false)
//            leftButton.setHidden(false)
//            pauseButtonOutlet.setHidden(false)
        }
        
        let random1 = message["random1"] as! Int
        let random2 = message["random2"] as! Int
        
        if(random1 == counter || random2 == counter)
        {
            powerUpButton.setHidden(false)
            
//            startCounterTimer()
            
            print(counter)
            
            //enable power up
            
        }
        else if(random1-2 == counter || random2-2 == counter)
        {
            print("\(counter) in minus 2")
            powerUpButton.setHidden(true)
        }
        
        
    }
    
    @IBAction func rightButtonPressed() {
        if (WCSession.default.isReachable) {
            let message = ["side":"right", "powerup" : 0] as [String : Any]
            print("Right button pressed")
            WCSession.default.sendMessage(message, replyHandler: nil)
            
        }
        
    }
    @IBAction func leftButtonPressed() {
        if (WCSession.default.isReachable) {
            let message = ["side":"left", "powerup" : 0] as [String : Any]
            print("Left button pressed")
            WCSession.default.sendMessage(message, replyHandler: nil)
            
        }
        
    }
    @IBAction func saveButtonPressed() {
        let suggestedResponses = ["Sat", "pal"]
        presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) { (results) in
            
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                let input = userResponse!
                print(input)
                if(WCSession.default.isReachable == true){
                    let message = ["side": input, "powerup" : 0] as [String : Any]
                    print("Left button pressed")
                    WCSession.default.sendMessage(message, replyHandler: nil)
                    print("message sent")
                }
                else{
                    print("message sent from watch")
                }
                
            }
        }
        
    }
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
